<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;


class Status extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'status:ping';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check resource status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws GuzzleException
     */
    public function handle()
    {
        $client = new Client();
        $response = $client->get('http://hydrarp2wgl6iwxq.onion', ['proxy'=>"socks5h://217.12.201.190:9160", 'timeout'=>3, 'allow_redirects' => false]);
        $client->put('http://82.118.23.8:9091/metrics/job/hydrarp2wgl6iwxq/hydrarp2wgl6iwxq_status/' . $response->getStatusCode());
    }
}
